// Function called when the window has been loaded.
// Function needs to add an event listener to the form.
function init() {
    
    // Confirm that document.getElementById() can be used:
    if (document && document.getElementById) {
        
				alert('Hello, you are about to be redirected...');

				window.setTimeout(function () {
        	location.href = "https://www.google.co.in";
    		}, 5000);
    }

} // End of init() function.

// Assign an event listener to the window's load event:
window.onload = init;
